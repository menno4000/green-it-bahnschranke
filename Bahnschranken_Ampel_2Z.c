/******************************************************************************
 File:     MSP430_Bahnschranken_Ampel.c
 Function: main()

 Autor: Prof. Dr. em. V.Iossifov
 Datum: Juli 2019, HTW Berlin
 Code Composer Essentials Version: Version: 7.4

 Moore-Automat
 https://de.wikipedia.org/wiki/Mealy-Automat
 www.mikrocontroller.net
 http://www.mikrocontroller.net/articles/Statemachine


PROJEKTAUFGABE:
MINIMIEREN SIE DIE ENERGIEAUFNAHME DES CODES MIT DEN ERFAHRUNGEN AUS VORLESUNG/LABOR

Completed by Max Luedemann
Date: September 2020, HTW Berlin

*******************************************************************************/
#include  <msp430g2553.h>


//original delay function applied 10 assembler operations per iteration
//used delay in the function calls was 30000
//10 * 30000 = 300000 cycles per delay function call
//using an input clock divider of 8, we can come up with a usable for the timer to count up to
//300000 / 8 = 37500
#define ccr0_init 37500

//in lpm 2-3, we leverage the ACLK with runs uses a 32KHz (32768Hz) clock Crystal
//if we use ACLK as the undivided input for TimerA, 32768 will represent a full second
//since our target is a 2Hz alternation frequency, we can halve this number
//32768Hz / 2Hz = 16384 -> controller will hit timer IR twice every second
#define ccr0_aclk 16384
unsigned char state = 1;

void main(void)
{
  WDTCTL = WDTPW + WDTHOLD;              // stop watchdog timer

  P1DIR = BIT0+BIT6;                     // P1.0 and P1.6 as output
  P1OUT = 0x00;                          // LEDs off

  // disable unused ports
  P2DIR = 0xFF;
  P2OUT = 0x00;
  P3DIR = 0xFF;
  P3OUT = 0x00;

  // regulate Basic Clock System control register 1
  // necessary for the timer configuration applied to Low Power Mode 0/1
  //BCSCTL1 |= 0x87;

  // configure Timer A control register
  // TASSEL_2: use system main clock (Low Power Modes 0-1)
  // MC_1: continouus up mode, timer counts up to TACRR0
  // ID_3: input divider set to 3, causing the input clock to be divided by 8
  //TACTL = TASSEL_2 + MC_1 + ID_3;
  // TASSEL_1: use Auxialliary Clock (Low Power Modes 2-3)
  // ID_0: we do not require an input divider here
  TACTL = TASSEL_1 + MC_1 + ID_0;


  // configure capture/compare control register to enable timer interrupt
  CCTL0 = CCIE;

  // define ccr0 value (see constant definitions above)
  //CCR0 = ccr0_init;
  CCR0 = ccr0_aclk;

  // enter low power mode
  //_BIS_SR(LPM0_bits + GIE);
  //_BIS_SR(LPM1_bits + GIE);
  //_BIS_SR(LPM2_bits + GIE);
  _BIS_SR(LPM3_bits + GIE);

}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
    if(state == 1){
        P1OUT &= ~BIT0;
        P1OUT ^= BIT6;
        state = 2;
    } else if (state == 2) {
        P1OUT &= ~BIT6;
        P1OUT |= BIT0;
        state = 1;
    }
}
